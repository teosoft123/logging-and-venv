import logging
import os
import unittest

import yaml

import logging_demo
from logging_demo import main

logger = logging.getLogger(__name__)


class TestLoggingFromTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        super(TestLoggingFromTest, cls).setUpClass()
        from logging.config import dictConfig

        with open(os.path.dirname(logging_demo.__file__) + '/logging-config.yaml', 'r') as yaml_file:
            config = yaml.load(yaml_file, Loader=yaml.FullLoader)

        config['version'] = 1
        dictConfig(config)

        logger.info("%s called as a script", __file__)

    def test_logging(self):
        logger.info("In test_logging")
        main()
