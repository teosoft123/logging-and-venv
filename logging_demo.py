import logging

from consumer.consumer import consume
from producer.producer import produce

# get logger here, set by application or script
logger = logging.getLogger(__name__)


def main():
    logging.info('Entering main()')
    message = produce()
    consume(message)


if __name__ == '__main__':
    # configure logging here, we're called as an application or script
    from logging.config import fileConfig

    fileConfig('logging-config.ini')
    logger.info("%s called as a script", __file__)
    main()
