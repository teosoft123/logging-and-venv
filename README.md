## Goals

Demonstrate logging and use of virtual environments in Python projects

See [README-LOGGING.md](README-LOGGING.md) for details about logging

## How to create a virtual environment

Run the following in the project root

    bash ./create-user-venv.sh

Activate your new virtual environment

source ${HOME}/${USER}-venv/bin/activate

Note: while you do have this line at the end of create-user-venv.sh, it only
activates the python virtual environment inside the script. When script exits,
virtual environment is not activated and you need to run the above command. (ONCE)
    
## How to run code in command line

Run the following in the project root

    python2 ./main.py
    
Note that you can run this without activating virtual environment because there is no
dependencies required by this simple code. However if you need 3rd party dependencies (modules, libraries)
you will install them after activating your virtual environment.
This will cause the dependencies to be installed into the virtual environment directory,
while the original Python installation directories will not receive any additional files coming form dependencies.

This is very helpful when you have project that use incompatible dependencies, but the most important,

**virtual environment keeps your original installation clean** 

Let's run the same code in your python virtual environment.
Open a new shell and change to the project root directory, and run this:

    source ${HOME}/${USER}-venv/bin/activate
    python ./main.py

Note that you don't need to qualify python with version, ie don't have to use python2.
This is because in your virtual environment python is a link to a real interpreter,  
the one provided when virtual environment is created.
    
## How to deactivate my virtual environment?

Glad you asked! Indeed, you might want to get out of the virtual environment you just 
activated, or activate another one, without leaving your current shell session. 
There's a special command that is only available when virtual environment is activated, 
and it's called `deativate`. Use it like this:

    [~] source ${HOME}/${USER}-venv/bin/activate
    # other commands
    (otsvinev-venv) [~] deactivate
    [~]      
    
## What else?

Try modifying PYTHON variable value at the beginning of the create-user-venv.sh script

Try changing basic logging configuration in main.py

## How to clean up my virtual environment?

If you don't need it, don't want it or something went wrong, delete 
the virtual environment directory, like so:

    rm -rf <full-path-to-virtual-environment-directory> 
