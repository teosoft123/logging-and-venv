#!/usr/bin/env bash

# This will create a virtual environment named after logged in user
# Let's select the Python installation we'll work with

# Do we have it?
which python2.7

# I assume we do - it comes with most *nix systems pre-installed

PYTHON=python2.7
${PYTHON} -V

# Now we need to make sure we have pip, the Python package manager

PIP=pip2.7
${PIP} -V

# If using Apple Python, you need to be root
sudo ${PIP} install virtualenv

# Show virtualenv version:
${PYTHON} -m virtualenv --version

# Let's create a vitrual environment
${PYTHON} -m virtualenv ${HOME}/${USER}-venv/

# Finally, let's activate it
# It's a manual operation
source ${HOME}/${USER}-venv/bin/activate

