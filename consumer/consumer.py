from __future__ import print_function
import logging

logger = logging.getLogger(__name__)


def consume(m):
    logger.info("in %s", __name__)
    logger.debug("consuming message %r", m)
    print("message: %s" % (m,))
