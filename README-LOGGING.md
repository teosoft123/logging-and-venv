## Goals

Demonstrate logging with configuration

* Has 2 packages and 3 modules, `main`, `producer` and `consumer`
* Each module is configured to use a separate `logger`
* Most modules use the same stream, but consumer is configured to use another, with different formatter
* Each logger can have its own logging level
* All loggers sent logs to `stdout`, but consumer sends them to `stderr`
  Because of this, logs might appear out of order. Try adding delays.


### Running from command line


#### Tests

In project root, run

    python logging_demo.py

In `tests` directory, run 

    PYTHONPATH=../ python -m unittest test_setup_logging_from_test.TestLoggingFromTest
  


