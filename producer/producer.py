import logging

logger = logging.getLogger(__name__)


def produce():
    logger.info("in %s", __name__)
    m = 'Hello world.'
    logger.debug("producing message %r", m)
    return m
